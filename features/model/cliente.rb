#Classe que contem todas as informações de cliente
class Cliente
  
  # id : integer
  attr_accessor :id
  # titulo do livro : String
  attr_accessor :userName
  # Descricao : String
  attr_accessor :password

  def initialize(usuario, senha)
    @userName = usuario
    @password = senha
  end

  # Define um modelo para cliente
  # ====== @params
  # -
  # ====== @return
  #  modelo
  def model
    @model = {
      id: id,
      userName: @userName,
      password: @password
    }
  end

  def id
    DateTime.now.strftime("%Y%m%d%k%M%S%L").to_i
  end

  def as_json(options={})
    model
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
  end

  def to_openStruct(json)
    return JSON.parse(json, object_class: OpenStruct)
  end

end