#language: pt
#utf-8

@api
Funcionalidade: API Clientes
  Eu como usuário
  Quero realizar cadastro no sistema
  Para isso a API de clientes deve estar funcionando

@positivo @post
Cenário: Envio de requisição post para cadastro de cliente com sucesso
  Dado que eu possua um cliente para cadastro com usuario "teste" e senha "123456"
  Quando eu realizar uma chamada de post com o cliente atual
  Entao sera retornado codigo de resposta 200