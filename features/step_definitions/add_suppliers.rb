Quando("preencher os campos de supplier com os dados de {string}") do |massa|
  @supplier = OpenStruct.new(MASS[massa.upcase])
  @supplier.email = @supplier.email.gsub('{id}',DateTime.now.strftime("%Y%m%d%k%M%S%L").to_s)
  @add_suppliers.preencher_formulario(@supplier)
end

Quando("submeter os dados do novo supplier") do
  @add_suppliers.button_submit.click
end