Dado("que eu realize uma requisição de GET") do
  @rest_client = RestService.new
  @rest_client.url = ENVIRONMENT['API']['WEBSERVICE']['get']
  @response = @rest_client.get
end
  
Entao("receberei status {int}") do |code|
  @response.code.should be == code
end

Entao("as informacoes de id e title dos itens completos devem ser exibidas") do
  @rest_client.to_openStruct(@response.body).each do |placeholder|
    if placeholder.completed == true
      puts "ID: " + placeholder.id.to_s
      puts "TITLE: " +  placeholder.to_h[:title]
      puts "------------------------------------"
    end
  end
end