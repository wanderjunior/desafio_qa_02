Quando("eu realizar uma chamada de post com o cliente atual") do
  @rest_client = RestService.new
  @rest_client.url = ENVIRONMENT['API']['WEBSERVICE']['post']
  @response = @rest_client.custom_post(payload: @cliente.to_json)
  puts "JSON ENVIADO: "
  puts JSON.pretty_generate(JSON.parse(@cliente.to_json))
end

Entao("sera retornado codigo de resposta {int}") do |code|
  @response.code.should be == code
end