Quando("clicar em adicionar novo supplier") do
  @supplier_management.button_add.click
  @add_suppliers = App.new.add_supplier
  @add_suppliers.wait_for_input_first_name
end

Entao("o novo supplier deve ser exibido na tela de suppliers management") do
  @supplier_management.validar_supplier(@supplier)
end