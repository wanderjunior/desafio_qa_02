Dado("que eu esteja na pagina inicial de login administrativo") do
  visit ENVIRONMENT['APP']['home']
  @login = App.new.login
  @login.wait_for_input_usuario
end

Dado("que eu possua um cliente para cadastro com usuario {string} e senha {string}") do |usuario, senha|
  @cliente = Cliente.new(usuario, senha)
end