Quando("eu realizar login com usuario {string}") do |massa|
  @usuario = OpenStruct.new(MASS[massa.upcase])
  @login.fazer_login(@usuario.usuario, @usuario.senha)
  @home = App.new.home
  @home.wait_for_span_user
end