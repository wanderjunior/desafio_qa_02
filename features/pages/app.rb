class App
  
  def login
    Login.new
  end

  def home
    Home.new
  end

  def supplier_management
    SupplierManagement.new
  end

  def add_supplier
    AddSupplier.new
  end
end