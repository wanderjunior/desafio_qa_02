class SupplierManagement < SitePrism::Page
  
  element :label_supplier_management, :css, '#content > div > div.panel-heading'
  element :button_add, :css, 'button[type="submit"]'

  def validar_supplier(supplier)
    r = true
    x = 0
    linhas_tabela = page.all('tr.xcrud-row')
    linhas_tabela.each do |linha|
      r = false if page.all('tr > td:nth-child(3)')[x].text != supplier.first_name
      r = false if page.all('tr > td:nth-child(4)')[x].text != supplier.last_name
      r = false if page.all('tr > td:nth-child(5)')[x].text != supplier.email
      x += 1
      return true if r
      r = true
    end
  end

end
