class AddSupplier < SitePrism::Page

  element :input_first_name, :css, 'input[name="fname"]'
  element :input_last_name, :css, 'input[name="lname"]'
  element :input_email, :css, 'input[name="email"]'
  element :input_password, :css, 'input[name="password"]'
  element :input_mobile_number, :css, 'input[name="mobile"]'
  element :input_address1, :css, 'input[name="address1"]'
  element :input_address2, :css, 'input[name="address2"]'
  element :button_submit, :css, 'button[class="btn btn-primary"]'

  def preencher_formulario(supplier)
    input_first_name.set supplier.first_name
    input_last_name.set supplier.last_name
    input_email.set supplier.email
    input_password.set supplier.password
    input_mobile_number.set supplier.mobile_number
    page.find("option[value='#{supplier.country}'").select_option
    input_address1.set supplier.address1
    input_address2.set supplier.address2
  end

end
