class Login < SitePrism::Page
  
  element :input_usuario, :css, 'input[type="text"]'
  element :input_senha, :css, 'input[name="password"]'
  element :button_login, :css, 'button[type="submit"]'

  def fazer_login(usuario, senha)
    input_usuario.set usuario
    input_senha.set senha
    button_login.click
  end

end
