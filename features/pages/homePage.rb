class Home < SitePrism::Page
  
  element :span_user, :css, 'div.user > span'
  element :link_suppliers_management, :css, 'a[href="https://www.phptravels.net/admin/accounts/suppliers/"]'
  element :button_accounts, :css, 'a[href="#ACCOUNTS"]'

  def acessar_tela_suppliers_management
    button_accounts.click
    wait_for_link_suppliers_management
    link_suppliers_management.click
  end

end

