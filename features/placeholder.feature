#language: pt
#utf-8

@api
Funcionalidade: Placeholder
  Validar retorno de chamada GET

@positivo @get
Cenário: Validar status code
  Dado que eu realize uma requisição de GET
  Entao receberei status 200
  E as informacoes de id e title dos itens completos devem ser exibidas
