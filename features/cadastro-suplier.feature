#language: pt
#utf-8

@cadastro @app
Funcionalidade: Cadastro Supplier
  Eu como administrador
  Quero realizar cadastro de funcionarios no sistema
  Para que os mesmos possam utilizá-lo

Contexto:
  Dado que eu esteja na pagina inicial de login administrativo
  Quando eu realizar login com usuario "admin"

@positivo
Cenário: Realizar cadastro de funcionario com sucesso
  Quando acessar a tela de suppliers management
  E clicar em adicionar novo supplier
  E preencher os campos de supplier com os dados de "supplier"
  E submeter os dados do novo supplier
  Entao o novo supplier deve ser exibido na tela de suppliers management
